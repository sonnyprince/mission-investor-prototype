(function ($) {

    var genericFunctions = function() {
    	console.log('loaded');
		$('.video-gal-open').on('click', function(e) {
			$('.video-gallery-overlay').toggleClass('open');
			e.preventDefault();
		});
		$('.video-gallery-overlay-close').on('click', function(e) {
			$('.video-gallery-overlay').toggleClass('open');
			e.preventDefault();
		});
		$('.child-sub-nav-toggle').on('click', function(e) {
			$('.child-sub-nav').slideToggle();
			e.preventDefault();
		});
    };
    genericFunctions();

})(jQuery);